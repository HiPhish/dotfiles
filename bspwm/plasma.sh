# KDE Plasma-specific settings for BSPWM

# Add padding to accommodate for the Plasma panels; the size is computed as a
# fraction of the current screen resolution

xrandr --current \
	| head -n 1 \
	| sed -E 's/.*current ([0-9]+) x ([0-9]+).*/\1/' \
	| xargs -ix -- expr x / 45 \
	| xargs -ix -- bspc config right_padding x

xrandr --current \
	| head -n 1 \
	| sed -E 's/.*current ([0-9]+) x ([0-9]+).*/\2/' \
	| xargs -iy -- expr y / 45 \
	| xargs -iy -- bspc config top_padding y


# Plasma floating overlays
bspc rule -a 'plasmashell' \
	state=floating focus=off sticky=on border=off center=off
bspc rule -a krunner border=off
