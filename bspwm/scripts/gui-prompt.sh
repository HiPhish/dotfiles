#!/bin/bash
# A graphical prompt for BSPWM: open a text input GUI dialog, pass the entered
# text as CLI parameters to bspc, then display feedback if necessary.
#
# Examples:
#   node -f north     (move focus north, play error sound if no northern node)
#   query --names -D  (list all desktops by name)
#   wm -r             (restart the window manager)


# ===[ VARIABLES ]=============================================================
error_sound=/usr/share/sounds/freedesktop/stereo/dialog-error.oga


# =============================================================================
instruction=$(kdialog --title 'BSPC' --inputbox 'BSPC command' '')
[ $? -ne 0 ] && exit

output="$(bspc $instruction 2>&1)"
success=$?

if [ -z "$output" ] ; then
	[ "$success" -ne 0 ] && paplay "$error_sound"
	exit
fi

dialog=$([ "$success" -eq 0 ] && printf 'msgbox' || printf 'error')
kdialog --title 'BSPWM' --$dialog "$output"
