# Special rules for various applications go in this file.

bspc rule -a 'pinentry'      state=floating
bspc rule -a 'DBeaver'       state=tiled
bspc rule -a 'kcalc'         state=floating
bspc rule -a 'krunner'       state=floating
bspc rule -a 'systemmonitor' state=floating
bspc rule -a 'spectacle'     state=floating
bspc rule -a 'vokoscreen'    state=floating

bspc rule -a 'Java:*:DBeaver ' state=floating
bspc rule -a 'DBeaver:*:Version update' state=floating
bspc rule -a 'DBeaver:*:Preferences ' state=floating
bspc rule -a 'DBeaver:*:Connect to a database ' state=floating
bspc rule -a 'DBeaver:*:Can'\''t connect to database ' state=floating

bspc rule -a 'Spyder:*:Text editor - Profiler output' state=floating
