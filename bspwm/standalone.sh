# BSPWM setting for use without any particular desktop environment


# ===[ VARIABLES ]=============================================================
WALLPAPER=~/.local/share/wallpapers/853-FHD.jpg


# ===[ AUTOSTART ]=============================================================
which feh && feh --no-fehbg --bg-center "$WALLPAPER"
