TODOs and further ideas
#######################

- When the child is closed the parent needs to take its position in the tree
- When the child is closed the parent needs to have the same size


Bugs and other problems
-----------------------

- If the script crashes hidden windows will be stuck
- The parent does not always follow the child
