# Automatic BSPC window swallowing and spitting
# =============================================
#
# Swallowing is when a window temporarily takes the place of a previous window
# in the tree. For example, a terminal might spawn a GUI window for another
# application. There is no use for the terminal window to be on the screen
# while the user is using the GUI application, so it makes sense for the GUI
# window to "replace" the terminal window.
#
# Spitting the inverse operation: when the GUI window is closed we want the
# terminal window to be put back in place.
#
# The combination of swallowing and spitting gives the illusion that the
# terminal temporarily turns into another application and then reverts back
# when the work is done.
#
#
# Using
# -----
# Requires a POSIX compliant Awk implementation.
#
#   $ bspc subscribe node | awk -f <this script>


# Edit settings by adding entries to these arrays. The value does not matter,
# only the key does.
BEGIN {
	# Applications whose windows can be swallowed, usually terminal emulators
	# The value must be the same as the second string in the WM_CLASS property
	# of an application window.
	edible["Alacritty"]

	# Applications or window titles for which to not swallow the parent
	except["Event Tester"]
}

# Setting up automatic global variables and restoring state. There are two
# variables which keep the state: the stomach and the mouth. The stomach maps a
# child process PID to a swallowed parent process window. The mouth maps a
# visible window to its PID.
BEGIN {
	cache_dir = ("XDG_CACHE_HOME" in ENVIRON ? ENVIRON["XDG_CACHE_HOME"] : ENVIRON["HOME"]"/.cache") "/bspwm/swallow"
	ruminate(cache_dir"/cud.csv")
}

/^node_add/ && nwindows(wm_pid($5)) {  # New node is $5
	mouth[$5] = wm_pid($5)
	next
}

# Swallow a window which has spawned another window
/^node_add/ {  # New node is $5
	prev = query("bspc query -N -d -n last.window")

	if (!(wm_class(prev) in edible)) 
		next

	if (wm_class($5) in except || wm_name($5) in except)
		next

	if (is_floating($5) || is_floating(prev)) 
		next

	new_pid = wm_pid($5)
	old_pid = wm_pid(prev)
	if (!is_child_of(new_pid, old_pid))
		next

	system("bspc node --swap " prev " --follow")
	system("bspc node " $5   " --flag private=on")
	system("bspc node " prev " --flag hidden=on")
	system("bspc node " prev " --flag private=on")

	stomach[new_pid] = prev
	mouth[$5] = new_pid
}

# Spit out a window which had been swallowed previously
/^node_remove/ && $4 in mouth {  # Removed node is $4
	pid = mouth[$4]
	delete mouth[$4]
	if (nwindows(pid))
		next

	swallowed = stomach[pid]
	delete stomach[pid]
	system("bspc node "swallowed" --flag hidden=off")
	system("bspc node "swallowed" --focus")
}

# Resize the swallowed node when the child node get resized
/^node_geometry/ && $4 in windows {  # Geometry is $5
	# Geometry format: (\d+)x(\d+)\+(\d+)\+(\d+)
	# There is no documentation on what the individual number mean, but here is
	# my guess:
	#   - $1: width
	#   - $2: height
	#   - $3: position of the left edge relative to the left edge of the screen
	#   - $4: position of the top edge relative to the top edge of the screen
	#
	# Get the geometry of an existing window from the _NET_WM_ICON_GEOMETRY
	# property.

	# bspc node --move
	# bspc node --resize
}

# Swallowed window needs to follow after its child
(/^node_swap/ || /^node_transfer/) && $4 in mouth {
	# # $5: Target monitor
	# # $6: Target desktop
	# # $7: Target node
	# print "transferred " $4
	node = stomach[$4]
	# print("bspc node "node" --to-monitor "$5)
	# print("bspc node "node" --to-desktop "$6)
	# print("bspc node "node" --to-node "$7)
}

# If there are any swallowed windows we need to dump the state to a file so it
# can be restored later.
END {
	if (arr_empy(stomach))
		exit

	system("mkdir -p "cache_dir)
	printf "" >cache_dir"/cud.csv"
	for (pid in stomach) {
		printf "%s,%s", pid, stomach[pid] >>cache_dir"/cud.csv"
		for (win in mouth) {
			if (mouth[win] == pid)
				printf ",%s", win >>cache_dir"/cud.csv"
		}
		printf "\n" >>cache_dir"/cud.csv"
	}
	close(cache_dir"/cud.csv")
}


# ===[ FUNCTIONS ]=============================================================
# In some cases we call the system function for its return value as a boolean.
# System returns zero on success, so we have to invert the return value first.

# Run a query command, return the output as a string.
function query(cmd, _out) {
	cmd | getline _out
	close(cmd)
	return _out
}

# Returns the X window property. See the manual of xprop(1) for the format and
# dformat options
function wm_prop(prop, format, dformat, wid) {
	# We still need to strip the property name and surrounding quotes from the
	# field using sed
	return query("xprop -id "wid" -notype '"format"' '"dformat"' '"prop"' | sed 's/^"prop"\"//; s/\"$//'")
}

# Determine a process ID from a window ID.
function wm_pid(wid) {
	return query("xprop -id "wid" -notype '32c' ' $0\n' '_NET_WM_PID' | cut -d ' ' -f 2")
}

# Returns the application name (second field) of the window.
function wm_class(wid) {
	return wm_prop("WM_CLASS", "8s", "$1\n", wid)
}

function wm_name(wid) {
	return wm_prop("WM_NAME", "8s", "$0\n", wid)
}

# Find out whether p1 is a child process of p2
function is_child_of(p1, p2) {
	return p1 && !system("[ $(pstree -T -p " p2 " | grep '" p1 "' | wc -l) -gt 0 ]")
}

# Determine whether the window with a given ID is a floating window
function is_floating(wid) {
	return !system("bspc query -N -n " wid ".floating")
}

function nwindows(pid, _i, _win) {
	for (_win in mouth) {
		if (mouth[_win] == pid) ++_i
	}
	return _i
}

# Returns true if the array has at least one key inside it, false otherwise
function arr_empy(arr, _) {
	for (_ in arr)
		return 0
	return 1
}

# Restore the state from a CSV file on disc
function ruminate(cud, _chunk, _entry, _items, _i) {
	if (system("[ -f  "cud" ]"))
		return

	while (getline _chunk <cud > 0) {
		_items = split(_chunk, _entry, ",")
		if (_items < 2) {
			# Is this the correct way of printing to standard error? Do I have
			# to close the pipe afterwards?
			# print "BSPWM swallow: malformed cud entry \""chunk"\"" | "cat 1>&2"
			print "BSPWM swallow: malformed cud entry :"_chunk
			continue  # Skip malformed entries
		}

		# Check if the PID even exists
		if (system("ps -p "_entry[1]" >/dev/null"))
			continue

		stomach[_entry[1]] = _entry[2]
		for (_i = 3; _i <= _items; ++_i) {
			# Add the node to the mouth, unless it has been closed in the
			# meantime
			if (!system("bspc query -N -n "_entry[_i]))
				mouth[_entry[_i]] = _entry[1]
		}
	}

	close(cud)
	system("rm -f "cud)
}
