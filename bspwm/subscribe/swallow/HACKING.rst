.. default-role:: code

########################################
 Hacking on the BSPWM swallowing script
########################################


Terminology
###########

The terminology is inspired by the biology of ruminant animals.

swallowing
   Temporarily hiding a window from display while another process's windows are
   being displayed. Swallowed windows are mean to be spit back out eventually.

spitting
   Unhiding a swallowed window.

stomach
   Associative array mapping the PID of a child process to a swallowed window
   of the parent process.

mouth
   Associative array mapping a displayed window to its PID. Used to keep track
   of the open windows.

cud
   On-disc representation of the current state of the script. When the script
   terminates while there are still swallowed windows we write all the relevant
   information to disc so we can reload it later.

ruminating
   The act of restoring a previous state from the script.
