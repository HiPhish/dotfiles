.. default-role:: code

#############################
 Window swallowing for BSPWM
#############################

This plugin for the BSPWM_ tiling window manager implements window swallowing.
Imagine you are using a terminal emulator and you start a graphical application
with its own window. The terminal window remains on screen, taking up space for
no good reason. Swallowing temporarily hides the terminal window until you
close the window of the child process, then it spits the terminal back out.

Highlights include:

- Handles processes with multiple windows
- Survives window manager restarts


Requirements
############

You need BSPWM and a POSIX-compliant implementation of Awk.


Usage
#####

The script reads node events from `bspc subscribe` and processes them.

.. code-block:: sh
   
   bspc subscribe node | awk -f swallow.awk

Alternatively you may want to insert a filter in-between to pass only relevant
events to the script. Awk is generally slower than grep, so filtering the
events first might improve performance somewhat.

.. code-block:: sh

   bspc subscribe node | grep --line-buffered -f events.grep | awk -f swallow.awk

The `--line-buffered` option is needed if your grep implementation buffers its
output. Without it the events won't arrive at Awk until enough output has built
up.


Shortcomings
############

- When the script spits out a swallowed window, the window does not take on the
  position and size of the window that was just closed.
- There is no way to not swallow a terminal window when you want to keep the
  terminal visible.


License
#######

Released under the terms of the MIT (Expat) license, please see the COPYING_
file for details.

.. _BSPWM: https://github.com/baskerville/bspwm
.. _COPYING: COPYING.txt
