# Change the border colour of the active window depending on whether it is the
# only window on the screen or not. Having the focus colour on the only window
# is distracting, so this script alter the active colour between the default
# active and default inactive colour as necessary.

BEGIN {
	# Store the BSPWM settings in a variable for later reference
	"bspc config focused_border_color" | getline focus
	"bspc config  normal_border_color" | getline normal
	close("bspc config focused_border_color" )
	close("bspc config  normal_border_color" )
}

# Using a special highlight for the focused node when there is only one node is
# distracting; use the same border colour as for nodes out of focus.
/^node_add/ || /^node_remove/ || /^node_focus/ {
	if (system("[ $(bspc query -N -m focused -d focused -n .normal | wc -l) -le 1 ]"))
		colour = focus
	else
		colour = normal
	system("bspc config -d focused focused_border_color '" colour "'")
}
