.. default-role:: code

##############################################################
 Making SBCL comply with the XDG base directory specification
##############################################################

By default SBCL uses the user-specific file `~/.sbcl` for initialisation.
However, we can edit the global file `/etc/sbclrc` in order to change this
behaviour. Append the following code to the global initialisation file:

.. code-block:: lisp

   (require :asdf)
   (let ((default-init-file (funcall sb-ext:*userinit-pathname-function*)))
      (unless (or (null default-init-file)
                  (typep default-init-file 'stream)
                  (uiop:file-exists-p default-init-file))
        (setf sb-ext:*userinit-pathname-function*
              (lambda () (uiop:xdg-config-home #P"sbcl/init.lisp")))))


The important part is the `sb-ext:*userinit-pathname-function*` variable: it is
bound to a function which returns one of the following:

- A pathname designator to the initialization file to load
- A stream to read initialization code from
- `NIL` (do not load any external initialization code)

Since the file will be used by all users we make sure to first check that the
function does indeed return a pathname designator, and that the file does not
already exist. This makes sure that if a user already has a `~/.sbclrc` file it
will take precedence.
