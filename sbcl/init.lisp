;;; The following lines added by ql:add-to-init-file:
#-quicklisp
(let ((quicklisp-init (merge-pathnames "common-lisp/quicklisp/setup.lisp"
                                       (uiop:xdg-data-home))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))


;;; A REPL prompt similar to Allegro Common Lisp
(require :sb-aclrepl)
(push :aclrepl cl:*features*)
(setf sb-aclrepl:*max-history* 100)
(setf sb-aclrepl:*command-char* #\,)
(setf sb-aclrepl:*exit-on-eof* T)


;;; Initialise Linedit for more pleasant command-line REPL, silently fail if it
;;; is not available
(if (member "--no-linedit" sb-ext:*posix-argv* :test #'string-equal)
  (setf sb-ext:*posix-argv*
        (remove "--no-linedit" sb-ext:*posix-argv* :test #'string-equal))
  (when (and (interactive-stream-p *terminal-io*)
             (not (find-package :swank)))
    (handler-case
      (progn
        (require :linedit)
        (let* ((hist-file (uiop:xdg-cache-home #P"sbcl/linedit-history"))
               (hist-dir  (uiop:pathname-directory-pathname hist-file)))
          (unless (uiop:directory-exists-p hist-dir)
            (uiop:ensure-all-directories-exist (list hist-file)))
          (funcall (intern "INSTALL-REPL" :linedit)
                   :wrap-current T
                   :eof-quits T
                   ;; ~/.cache/sbcl/linedit-history
                   :history hist-file)))
      (sb-int:extension-failure ()
        nil))))
