#!/usr/bin/env bash
# Various custom bindings for FZF and bash


# =============================================================================
# Splice the name of a Git branch into the current command line. Does nothing
# if the current directory is not a Git repository.
__fzf_git_branch__ () {
	git status >/dev/null 2>&1 || return
	local FZF_DEFAULT_OPTS
	export FZF_DEFAULT_OPTS='--height=30% --min-height=7 --layout=reverse'
	branch=$(git branch --list | sed -E 's/^\*?\s*//' | fzf)
    READLINE_LINE="${READLINE_LINE:0:$READLINE_POINT}$branch${READLINE_LINE:$READLINE_POINT}"
    READLINE_POINT=$(( READLINE_POINT + ${#branch} ))
}


# =============================================================================
bind -x '"\C-g": "__fzf_git_branch__"'
