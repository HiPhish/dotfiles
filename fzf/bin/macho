#!/bin/sh

# macho: man on steroids
#
# Macho displays a FZF menu of manual pages, sorted by category, lets the user
# pick one of the manuals and displays it using the regular man command.

export FZF_DEFAULT_OPTS='
--height=30%
--tiebreak=begin,index
--layout=reverse
--prompt="Manual: "
--preview="echo {} | sed -E \"s/\\(([^,]+).*\\)	([^, ]+).*	.*/\\1 \\2/\" | xargs  man"'

help () {
	echo "$0 -- man on steroids"
	echo ""
	echo "Usage: macho [-s section]"
	echo "       macho ( -h | --help )"
}


# -----------------------------------------------------------------------------
[ "$*" = "-h" ] || [ "$*" = "--help" ] && help && exit 0

while getopts ":s:" opt; do
	case $opt in
		s ) SECTION=$OPTARG; shift; shift;;
		\?) echo "Invalid option: -$OPTARG" >&2; exit 1;;
		: ) echo "Option -$OPTARG requires an argument" >&2; exit 1;;
	esac
done


# -----------------------------------------------------------------------------
# Get the manual:
#   1) Display all manual entries
#   2) Re-format the line: match sections, names and descriptions, then
#      re-order them with tabs in-between
#   3) Sort by section
#   4) FZF with preview (see next step for the weird sed command)
#   5) Extract the section and name from parentheses; pick only the first one
#      of each
#
# Note: Sometimes there is more than one section or more than one name. In that
# case the individual sections or entries are separated by a comma and a space.
#
# Note: The preview uses cat as its pager and it has to run the parenthesized
# section through a pipeline to strip away the parentheses so that it can be
# used as the section number in the man command. We also redirect the standard
# error to /dev/null in order to suppress troff warnings.

manual=$(apropos -s ${SECTION:-''} ${@:-.} | \
	sed -E 's/^([^(]*)\s?\(([^)]*?)\)\s+-\s+(.*)/(\2)	\1 	- \3/' | \
	sort | \
	fzf | \
	sed -E 's/\(([^,]+).*\)	([^,]+).*	.*/\1 \2/')

[ -z "$manual" ] && exit 0
man $manual
