.. default-role: code

########################
 Java package directory
########################

This directory is for manually managed Java packages. The state of Java
packages in the Ubuntu repositories is often outdated, and Guix is lacking in
Java packages. The easiest solution is to just grab the pre-compiled packages
(or compile them myself) and manage them by hand.

Here is how it is all maintained: the `archive` directory contains archives of
the packages as they were downloaded. Packages are extracted into the `store`
directory where they just sit, where multiple versions of one package can
co-exist. To actually enable a package symlink its directory into the `active`
directory.

To actually get the system to follow this scheme the file `PROFILE.sh` must be
sourced by the shell. This script sets up all the environment variables to use
the activated directories.

Example directory structure:

.. code-block::

   ~/.java-stuff
   ├── README.rst
   ├── PROFILE.sh
   ├── .gitignore
   ├── archive
   │   ├── apache-maven-3.6.2-bin.tar.gz
   │   └── OpenJDK8U-jdk_x64_linux_hotspot_8u232b09.tar.gz
   ├── active
   │   ├── jdk8 -> ../store/jdk8u232-b09/
   │   └── maven -> ../store/apache-maven-3.6.2/
   └── store
       ├── apache-maven-3.6.2
       │   ├── bin
       │   │   ├── ...
       │   ├── LICENSE
       │   ├── NOTICE
       │   └── README.txt
       └── jdk8u232-b09
           ├── bin
           │   └── ...
           ├── include
           │   └── ...
           ├── lib
           │   └── ...
           └── man
               └── ...

This effectively means that this directory does not contain any package
information, it only provides the structure for adding and enabling packages.


Environment variables
#####################

Set and export these variables, then start up a new login shell. Example:

.. code-block:: sh

   # This is the default
   $ java -version
   openjdk version "11.0.6" 2020-01-14
   OpenJDK Runtime Environment AdoptOpenJDK (build 11.0.6+10)
   OpenJDK 64-Bit Server VM AdoptOpenJDK (build 11.0.6+10, mixed mode)
   # Start a new login shell with OpenJDK 8
   $ OPENJDK=openjdk8 bash -l
   $ java -version
   openjdk version "1.8.0_232"
   OpenJDK Runtime Environment (AdoptOpenJDK)(build 1.8.0_232-b09)
   OpenJDK 64-Bit Server VM (AdoptOpenJDK)(build 25.232-b09, mixed mode)

`OPENJDK`
   Directory name of the current OpenJDK in the active directory. Defaults to
   whatever I have chosen as my system default (usually the lastest LTS release).
