#!/bin/sh

# Source this file from your shell to set up all your Java-related stuff up.
# Guard every group with a directory check to prevent junk variables from being
# set.

# All active packages reside in this directory (ideally the script would
# determine its own directory instead of relying on a hard-coded path)
PREFIX=~/.java-stuff/active

# ===[ OpenJDK ]===============================================================
#   https://openjdk.java.net/
# Currently set OpenJDK version
openjdk=${OPENJDK:-openjdk11}

if [ -d "$PREFIX/$openjdk" ]; then
	export JAVA_HOME="$PREFIX/$openjdk"
	export PATH="$JAVA_HOME/bin:$PATH"
	export MANPATH="$JAVA_HOME/man:$MANPATH"
fi


# ===[ Groovy ]================================================================
#   https://groovy.apache.org/
if [[ -d $PREFIX/groovy ]]; then
	export PATH="$PREFIX/groovy/bin:$PATH"
fi

# ===[ Ant ]===================================================================
#   https://ant.apache.org/
if [ -d $PREFIX/apache-ant ]; then
	export PATH="$PREFIX/apache-ant/bin:$PATH"
fi


# ===[ Maven ]=================================================================
#   https://maven.apache.org/index.html

if [ -d $PREFIX/maven ]; then
	export PATH="$PREFIX/maven/bin:$PATH"
fi


# ===[ Gradle ]================================================================
#  https://gradle.org

if [ -d $PREFIX/gradle ]; then
	export PATH="$PREFIX/gradle/bin:$PATH"
fi


# ===[ Clojure ]===============================================================
