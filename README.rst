######################
 My personal dotfiles
######################

This is a collection of my own personal dotfiles which are too small to be
their own repos. Feel free to copy and adapt to your needs anything from here
without asking for permission even when there is no explicit license attached.


Usage
#####

The sub-directories contain the settings for individual programs. Clone this
repo to some location (I use `$HOME/.dotfiles`) and then symlink the individual
configurations to their expected places.

Note: each sub-directory can have its own `.gitignore` file if it needs one.
