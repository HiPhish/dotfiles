alias open=xdg-open

# Add colour to the following commands
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Shortcut for TOTP authentication
alias totp='oathtool --totp -b - <'

# Create a directory and cd into it in one go
enter () {
	mkdir -p "$1" && cd "$1" || return
}

# Start a new shell with virtual environment.  Default name is '.venv'.
venv() {
	local name="${1-.venv}"
	local initfile="${name}/bin/activate"
	if [[ -r "${initfile}" ]]; then
		bash --init-file "$initfile"
	else
		echo "No Python virtual environment named '${name}'" 1>&2
		false
	fi
}
