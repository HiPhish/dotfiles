.. default-role:: code

=============================
 XDG base directory for Bash
=============================

Bash does not support the `XDG base directory`_ specification, but we can
coerce it into doing so by modifying the global settings files. The following
has been tried on `Void Linux`_.

First let's take care of login shells. Create a file named
`/etc/profile.d/bash_xdg.sh`. This file will be sourced automatically according
to `/etc/profile`.

.. code-block:: sh

   # Make bash follow the XDG_CONFIG_HOME convention
   _confdir=${XDG_CONFIG_HOME:-$HOME/.config}/bash
   _datadir=${XDG_DATA_HOME:-$HOME/.local/share}/bash

   if [[ -d "$_confdir" ]]
   then
      for f in bash_profile bashrc; do
          [[ -r "$_confdir/$f" ]] && . "$_confdir/$f"
      done
   fi

   [[ ! -d "$_datadir" ]] && mkdir -p "$_datadir"
   HISTFILE="$_datadir/history"

   unset _confdir
   unset _datadir

Now we need to take care of interactive shells. Create a file named
`/etc/bash/bashrc.d/bash_xdg.sh`. This file will be sourced automatically
according to `/etc/bash/bashrc`.

.. code-block:: sh

   _confdir=${XDG_CONFIG_HOME:-$HOME/.config}/bash
   _datadir=${XDG_DATA_HOME:-$HOME/.local/share}/bash

   [[ -r "$_confdir/bashrc" ]] && . "$_confdir/bashrc"

   [[ ! -d "$_datadir" ]] && mkdir -p "$_datadir"
   HISTFILE="$_datadir/history"

   unset _confdir
   unset _datadir




.. ----------------------------------------------------------------------------
.. _XDG base directory: https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
.. _Void Linux: https://voidlinux.org/
