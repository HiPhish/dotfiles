# Pretty man pages
# https://felipec.wordpress.com/2021/06/05/adventures-with-man-color/
export MANPAGER="less -R --use-color -Dd+r -Du+g"
export MANROFFOPT="-c"
