# FZF (source bindings for apt- or guix-installed FZF)
[[ -f /usr/share/doc/fzf/examples/key-bindings.bash ]] && \
	source /usr/share/doc/fzf/examples/key-bindings.bash
[[ -f ~/.dotfiles/fzf/bindings.bash ]] && \
	source ~/.dotfiles/fzf/bindings.bash
# Add the FZF scripts to the end because I don't want them to override system
# commands
[[ -d ~/.dotfiles/fzf/bin && "$PATH" != *~/.dotfiles/fzf/bin* ]] \
	&& export PATH="$PATH:${HOME}/.dotfiles/fzf/bin"
