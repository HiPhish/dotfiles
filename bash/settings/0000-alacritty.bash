if [[ "${TERM}" = 'alacritty' ]]; then
	# Add Alacritty helper scripts to the path; we can bind keys to programs in
	# Alacritty, but the program has to be either an absolute path or its
	# directory must be in the $PATH.
	export PATH="${PATH}:${XDG_CONFIG_HOME:-~/.config}/alacritty/bin/"
fi
