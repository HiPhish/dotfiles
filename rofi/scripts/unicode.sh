#!/bin/sh
# Display a list of Unicode characters, copy selection to clipboard. The
# characters are sourced from files stored under the unicode directory. The
# file format for Unicode files is one entry per line, on each line the code
# point, the literal character and a description, all separated by a tab each.
# Comments (lines starting with '#') and blank lines are ignored.


# List of known categories
CATEGORIES="Math Operators\nGreek\nBox Drawing\nArrows\nBlock Elements"

help () {
	echo "Usage: $0 [ <option> ... ]"
	echo "       $0 ( -h | --help )"
	echo ""
	echo "Possible options:"
	echo "   -c <category>  The Unicode category to display"
}

# Prints a list of symbols ready for dmenu to stdout
list_symbols () {
	grep -v -e '^#' -e '^$' $FILE
}

# -----------------------------------------------------------------------------
# Process command-line parameters
[ "$*" = "-h" ] || [ "$*" = "--help" ] && help && exit 0

while getopts ":c:" opt; do
	case $opt in
		c ) CATEGORY=$OPTARG;;
		\?) echo "Invalid option: -$OPTARG" >&2; exit 1;;
		: ) echo "Option -$OPTARG requires an argument" >&2; exit 1;;
	esac
done

# -----------------------------------------------------------------------------
[ -z "$CATEGORY" ] && CATEGORY=$(echo "$CATEGORIES" | rofi -dmenu -i -p "Unicode category")
[ -z "$CATEGORY" ] && exit 0

# -----------------------------------------------------------------------------
# Map the category to a file; if the file does not exist abort
case $CATEGORY in
	"Math Operators") FILE=mathematical-operators;;
	"Greek"         ) FILE=greek;;
	"Box Drawing"   ) FILE=box-drawing;;
	"Arrows"        ) FILE=arrows;;
	"Block Elements") FILE=block-elements;;
esac

[ -z "$FILE" ] && echo "No file for category $CATEGORY" >&2 && exit 1

FILE=$(dirname $(realpath $0))/unicode/$FILE.txt

[ ! -f $FILE ] && echo "No Unicode file $FILE" >&2 && exit 2

# -----------------------------------------------------------------------------
# Prompt for the symbol, copy it to the clipboard
list_symbols $FILE \
	| rofi -dmenu -i -p "Unicode character" \
	| cut -d "	" -f 2 \
	| tr -d '\n' \
	| xclip -selection clipboard
