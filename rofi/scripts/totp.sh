#!/bin/sh
# Use pass(1) and pass-otp(1) to type out TOTPs.

totp_dir="${PASSWORD_STORE_DIR:-${HOME}/.pass}/totp"

# The cut(1) command cannot "keep all fields up to the last one", but it can
# "keep all fields starting at the first one", so we reverse the string.
# Furthermore, xdotool(1) expect ever key to be its own argument, so we pad the
# text with spaces to turn one string into many.
ls "${totp_dir}" \
	| rev | cut -d. -f2- | rev \
	| rofi -dmenu -i -p 'TOTP'\
	| xargs -I{} pass otp 'totp/{}' \
	| sed -e 's/./\0 /g' | xargs xdotool key
