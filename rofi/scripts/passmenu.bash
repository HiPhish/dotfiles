#!/usr/bin/env bash

# Enable ** to expand to all files, directories and sub-directories
shopt -s nullglob globstar

PREFIX=${PASSWORD_STORE_DIR-~/.password-store}

help () {
	echo "Usage: $0 [ <option> ... ]"
	echo "       $0 ( -h | --help )"
	echo ""
	echo "Possible options:"
	echo "   -t        Print the password to standard output"
	echo "   -p <dir>  Use 'dir' as password prefix (default is '~/.password-store')"
}


# -----------------------------------------------------------------------------
[ "$*" = "-h" ] || [ "$*" = "--help" ] && help && exit 0
while getopts ":tep:" opt; do
	case $opt in
		t ) TYPEIT=1;;
		p ) PREFIX=$OPTARG;;
		e ) EXTENDED=1;;
		\?) echo "Invalid option: -$OPTARG" >&2; exit 1;;
		: ) echo "Option -$OPTARG requires an argument" >&2; exit 1;;
	esac
done

# -----------------------------------------------------------------------------
password_files=( "$PREFIX"/**/*.gpg )
password_files=( "${password_files[@]#"$PREFIX"/}" )
password_files=( "${password_files[@]%.gpg}" )

password=$(printf '%s' "${password_files[@]}" | rofi -dmenu -sep '' -i -p "Password Store")

[[ -z $password ]] && exit 0

if [[ $TYPEIT -eq 0 ]]; then
	pass show -c "$password" 2>/dev/null
elif [[ $EXTENDED -eq 1 ]]; then
	pass show "$password" | tail -n +2 | rofi -dmenu -i -p "$password"
else
	pass show "$password" | { IFS= read -r pass; printf %s "$pass"; } |
		xdotool type --clearmodifiers --file -
fi
