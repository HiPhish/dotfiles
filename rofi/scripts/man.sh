#!/bin/sh
# Display a list of man pages and display it in a reader application

READER=zathura
FORMAT=pdf

help () {
	echo "Usage: $0 [ <option> ... ]"
	echo "       $0 ( -h | --help )"
	echo ""
	echo "Possible options:"
	echo "   -r <reader>  Reader application for the manual (default zathura)"
	echo "   -T <format>  File format of the manual (default pdf)"
	echo ""
	echo "The reader must be able to read from standard input when passed '-'"
	echo "as an argument."
}


# -----------------------------------------------------------------------------
[ "$*" = "-h" ] || [ "$*" = "--help" ] && help && exit 0

while getopts ":r:T:" opt; do
	case $opt in
		r ) READER=$OPTARG;;
		T ) FORMAT=$OPTARG;;
		\?) echo "Invalid option: -$OPTARG" >&2; exit 1;;
		: ) echo "Option -$OPTARG requires an argument" >&2; exit 1;;
	esac
done


# -----------------------------------------------------------------------------
# List all manuals, re-order section, name and description sort, and display.
# Remove the surrounding parentheses from the category of the selected manual.
manual=$(apropos . | \
	sed -E 's/^([^(]*)\s?\(([^)]*?)\)\s+-\s+(.*)/(\2)	\1 	- \3/' | \
	sort | \
	rofi -dmenu -i -p "Manual" -matching fuzzy -sync | \
	sed -E 's/\(([^,]+).*\)	([^,]+).*	.*/\1 \2/')

[ -z "$manual" ] && exit 0;

man -T$FORMAT $manual | $READER -
